FROM nginx:alpine
COPY conf.d /etc/nginx/conf.d
COPY nginx.conf /etc/nginx
EXPOSE 80